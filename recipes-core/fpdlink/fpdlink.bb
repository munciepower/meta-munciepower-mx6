LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://gpl-2.0.txt;md5=d41d8cd98f00b204e9800998ecf8427e"

RDEPENDS_${PN} += "bash imx-gpu-g2d"
DEPENDS += "imx-gpu-g2d update-rc.d-native"

SRC_URI += "file://mxc_v4l2_tvin.c \
            file://ipu.h \
            file://mxcfb.h \
            file://gpl-2.0.txt \
            file://fpdlink"

S = "${WORKDIR}"

do_compile(){
    ${CC} ${CFLAGS} ${LDFLAGS} mxc_v4l2_tvin.c -o mxc_v4l2_tvin -lg2d
}

do_install(){
    install -d ${D}/home/root/
    install -d ${D}/etc/init.d/
    install -m 0755 ${WORKDIR}/fpdlink ${D}/etc/init.d/fpdlink
    install -m 0755 ${WORKDIR}/mxc_v4l2_tvin ${D}/home/root/

    update-rc.d -r ${D} fpdlink start 03 S .
}

FILES_${PN} += "/etc/init.d/fpdlink /home/root/mxc_v4l2_tvin"


