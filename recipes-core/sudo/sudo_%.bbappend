FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://01_wheel"

do_install_append() {
    install -m 0440 ${WORKDIR}/01_wheel ${D}/etc/sudoers.d/01_wheel
}

FILES_${PN}_append += "/etc/sudoers.d/01_wheel"

