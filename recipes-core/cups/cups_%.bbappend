FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

RDEPENDS_${PN} += "bash"
DEPENDS += "update-rc.d-native"

SRC_URI += "file://printers.conf \
			file://cups \
            file://TLP_2824_Plus.ppd"

do_install_append_muncieuts() {
    install -m 0644 ${WORKDIR}/printers.conf ${D}/etc/cups/printers.conf
    install -m 0644 ${WORKDIR}/TLP_2824_Plus.ppd ${D}/etc/cups/ppd/TLP_2824_Plus.ppd

    install -d ${D}/etc/init.d/
    install -m 0751 ${WORKDIR}/cups ${D}/etc/init.d/cups
	
    update-rc.d -r ${D} cups start 80 5 . stop 02 0 1 6 .
}

FILES_${PN}_append_muncieuts += "/etc/cups/ppd/TLP_2824_Plus.ppd /etc/cups/printers.conf /etc/init.d/cups"

