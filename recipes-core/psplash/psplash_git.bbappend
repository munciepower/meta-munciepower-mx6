
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://omni_splash-img.h file://font.h file://0002-Multiline-text-support.patch"

SPLASH_IMAGES = "file://omni_splash-img.h;outsuffix=default"


do_configure_prepend() {
    cp ${WORKDIR}/font.h ${S}/radeon-font.h
    cp ${WORKDIR}/omni_splash-img.h ${S}/omni_splash-img.h
}

