FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://check_fs_advantage file://check_fs_uts"

do_install_append_advantage () {
	install -m 0644    ${WORKDIR}/check_fs_advantage	${D}${sysconfdir}/default/check_fs
}

do_install_append_muncieuts () {
	install -m 0644    ${WORKDIR}/check_fs_uts	${D}${sysconfdir}/default/check_fs
}

do_install_append () {

	update-rc.d -f -r ${D} checkroot.sh remove
	#rm ${D}${sysconfdir}/init.d/checkroot.sh
}

FILES_${PN}_append_advantage += "/etc/default/check_fs"

