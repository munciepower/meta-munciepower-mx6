#!/bin/sh
### BEGIN INIT INFO
# Provides:          mountall
# Required-Start:    mountvirtfs
# Required-Stop: 
# Default-Start:     S
# Default-Stop:
# Short-Description: Mount all filesystems.
# Description:
### END INIT INFO

. /etc/default/rcS

#
# Mount local filesystems in /etc/fstab. For some reason, people
# might want to mount "proc" several times, and mount -v complains
# about this. So we mount "proc" filesystems without -v.
#
test "$VERBOSE" != no && echo "Mounting local filesystems..."

#remount rootfs read only
if [ "$ROOTFS_READ_ONLY" = "yes" ] ; then
	mount -n -o remount,ro /
fi

#file system check
if test -f /etc/default/check_fs ; then
	exec 9< /etc/default/check_fs
	while read mnt type <&9
	do
		case "$type" in
			ext4)
				fsck.ext4 -p $mnt
				;;
			vfat|fat)
				fsck.fat -a $mnt
				;;
			*)
				echo "Cannot fsck $mnt, unknown type $type"
				;;

		esac
	done
	exec 0>&9 9>&-
fi

mount -at nonfs,nosmbfs,noncpfs 2>/dev/null

#
# We might have mounted something over /dev, see if /dev/initctl is there.
#
if test ! -p /dev/initctl
then
	rm -f /dev/initctl
	mknod -m 600 /dev/initctl p
fi
kill -USR1 1

#
# Execute swapon command again, in case we want to swap to
# a file on a now mounted filesystem.
#
[ -x /sbin/swapon ] && swapon -a

: exit 0

