FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://login.defs_shadow-sysroot"

do_install_append() {
    install -d ${D}/etc/
    install -m 0440 ${WORKDIR}/login.defs_shadow-sysroot ${D}/etc/login.defs
}

FILES_${PN}_append = "/etc /etc/login.defs"
