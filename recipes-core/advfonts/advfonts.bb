LICENSE = "UFLv1.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/UFL-1.0;md5=6ece08a2deec223c31b3efc813b955ba"

RDEPENDS_${PN} = ""

SRC_URI += "file://Ubuntu-B.ttf \
            file://Ubuntu-C.ttf \
            file://UFL-1.0"

do_preconfigure_prepend(){
    cp ${WORKDIR}/UFL-1.0 

}

do_install(){
    install -d ${D}/usr/share/fonts/
    install -m 0644 ${WORKDIR}/Ubuntu-B.ttf ${D}/usr/share/fonts/Ubuntu-B.ttf
    install -m 0644 ${WORKDIR}/Ubuntu-C.ttf ${D}/usr/share/fonts/Ubuntu-C.ttf
}

FILES_${PN} += "/usr/share/fonts/Ubuntu-*.ttf"
