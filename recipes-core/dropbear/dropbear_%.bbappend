
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://dropbear_advantage \
            file://dropbear_muncieuts"

do_install_append_advantage() {
    install -m 0644 ${WORKDIR}/dropbear_advantage ${D}/etc/default/dropbear
}

do_install_append_muncieuts() {
    install -m 0644 ${WORKDIR}/dropbear_muncieuts ${D}/etc/default/dropbear
}

FILES_${PN}_append += "/etc/default/dropbear"

