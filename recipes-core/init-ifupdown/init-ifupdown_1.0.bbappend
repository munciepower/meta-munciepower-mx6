FILESEXTRAPATHS_prepend:= "${THISDIR}/files:"
FILESEXTRAPATHS_prepend_advantage:= "${THISDIR}/advantage:"
FILESEXTRAPATHS_prepend_muncieuts:= "${THISDIR}/muncieuts:"

SRC_URI += "file://iptables.rules \
            file://iptables-restore"

do_install_append(){
    install -m 0751 ${WORKDIR}/iptables-restore ${D}/etc/network/if-pre-up.d/iptables-restore
    install -m 0640 ${WORKDIR}/iptables.rules ${D}/etc/iptables.rules
}

FILES_${PN} += "/etc/iptables.rules /etc/network/if-pre-up.d/iptables-restore"
