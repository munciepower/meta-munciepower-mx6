LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://gpl-2.0.txt;md5=2a2c96dad860d4f077d3fc4cde077464"

RDEPENDS_${PN} += "bash"
DEPENDS += ""

SRC_URI += "file://fboverlaytool.c \
            file://gpl-2.0.txt"

S = "${WORKDIR}"

do_compile(){
    ${CC} ${CFLAGS} ${LDFLAGS} fboverlaytool.c -o fboverlaytool
}

do_install(){
    install -d ${D}/usr/bin/
    install -m 0751 ${WORKDIR}/fboverlaytool ${D}/usr/bin/
}

FILES_${PN} += "/usr/bin/fboverlaytool"


