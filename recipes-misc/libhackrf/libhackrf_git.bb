SUMMARY = "Libhackrf"
HOMEPAGE = "https://github.com/mossmann/hackrf/wiki"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../COPYING;md5=1484b9683e3fc1dd9f5cf802a23fe67c"

DEPENDS = "libusb fftw"

PV = "0.5"

inherit cmake pkgconfig

SRC_URI = "git://github.com/mossmann/hackrf.git;branch=master \
          "
S = "${WORKDIR}/git/host"

SRCREV = "6e5cbda2945c3bab0e6e1510eae418eda60c358e"
