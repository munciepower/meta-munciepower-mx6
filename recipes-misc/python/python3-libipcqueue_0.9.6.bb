
SUMMARY = "Python library for IPC Queues"
HOMEPAGE = "https://github.com/seifert/ipcqueue"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3a0b90298694714553c9fafca7a20703"

SRC_URI += "file://00-IPCQueue-remove-serializer.patch"

PYPI_PACKAGE = "ipcqueue"

SRC_URI[md5sum] = "b88b048c9f26d162be7b1af28a84eeec"
SRC_URI[sha256sum] = "62c7ed68fd65cc91f964dbe6eb2a7199c02eb72dedea81999526365dd9b4560a"

inherit pypi setuptools3
