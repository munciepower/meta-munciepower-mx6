SUMMARY = "C++ class library of cryptographic schemes"
HOMEPAGE = "http://cryptopp.com"
SECTION = "libs"
LICENSE = "BSL-1.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSL-1.0;md5=65a7df9ad57aacf825fd252c4c33288c"

RDEPENDS_${PN} = ""

SRC_URI = "https://www.cryptopp.com/cryptopp820.zip"
SRC_URI[sha256sum] = "03f0e2242e11b9d19b28d0ec5a3fa8ed5cc7b27640e6bed365744f593e858058"

inherit autotools-brokensep pkgconfig

S = "${WORKDIR}"

do_compile() {
    oe_runmake -f GNUmakefile-cross clean
    oe_runmake -f GNUmakefile-cross dynamic CXXFLAGS="-DNDEBUG -g -O2 -fPIC"
}

do_install () {
    oe_runmake -f GNUmakefile-cross install-lib PREFIX=${D}/usr/

    mv ${D}/usr/lib/libcryptopp.so ${D}/usr/lib/libcryptopp.so.${PV}
    ln -s -r ${D}/usr/lib/libcryptopp.so.${PV} ${D}/usr/lib/libcryptopp.so
}
