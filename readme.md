# README #
This readme is intended for the meta-munciepower-mx6 layer, which is designed for the Advantage+ hardware.  This is designed to be run with the Yocto/Openembedded build system and depends on the following layers: meta, meta-poky, meta-oe, meta-multimedia, meta-freescale, meta-freescale-3rdparty, meta-networking, meta-python, meta-qt5, meta-variscite-fslc.

This was tested with the following layer/version combination (Yocto Sumo)
```
meta                 
meta-poky            = "HEAD:623b77885051174d0e05198843e739110977bd18"
meta-oe              
meta-multimedia      = "HEAD:8760facba1bceb299b3613b8955621ddaa3d4c3f"
meta-freescale       = "HEAD:407c6cf408969445031a492e2d25e0e2749582ea"
meta-freescale-3rdparty = "HEAD:88a29631809d1af0df618245430db29f2a7012b5"
meta-freescale-distro = "HEAD:f7e2216e93aff14ac32728a13637a48df436b7f4"
meta-browser         = "HEAD:75640e14e325479c076b6272b646be7a239c18aa"
meta-gnome           
meta-networking      
meta-python          = "HEAD:8760facba1bceb299b3613b8955621ddaa3d4c3f"
meta-qt5             = "HEAD:d4e7f73d04e8448d326b6f89908701e304e37d65"
meta-swupdate        = "HEAD:f2d65d87485ada5a2d3a744fd7b9e46ec7e6b9f2"
meta-variscite-fslc  = "HEAD:61cd8e06734e64bbf61888d2693b86e06dcae676"
meta-munciepower-mx6 = "HEAD:c1c5980a709896a9a4510b25dea311cb8ad66362"

```

See http://variwiki.com/index.php?title=Yocto_Build_Release&release=RELEASE_SUMO_V1.2_VAR-SOM-MX6 for more information on this process

## Initial Setup ##
* Ensure that your system complies with the Yocto Minimum requirements (https://docs.yoctoproject.org/ref-manual/system-requirements.html)
* Ensure that all required packages are installed (see above)
* Install and download repo for easy layer download
```
git config --global user.name "Your Name"
git config --global user.email "Your Email"
mkdir ~/bin
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
export PATH=~/bin:$PATH
```

* Use repo to download all required layers
```
mkdir yocto
cd yocto
repo init -u https://github.com/varigit/variscite-bsp-platform.git -b refs/tags/sumo-fslc-4.9.88-mx6-v1.2
repo sync -j4
```

* Download meta-munciepower-mx6 layer
```
 cd yocto/sources/
 git clone https://bitbucket.org/munciepower/meta-munciepower-mx6
```

## Configure Yocto ##
On initial use you should configure yocto as follows
```
cd yocto
MACHINE=advantage DISTRO=fslc-framebuffer . setup-environment fs_advantage
```

Also, the meta-munciepower-mx6 layer needs to be added to the configuration on initial setup
```
bitbake-layers add-layer ../sources/meta-munciepower-mx6
```

After this initial setup you can re-activate yocto by running the following:
```
cd yocto
. setup-environment fs_advantage
```

## Build OS Image ##
To build the OS image you can use the bitbake command in a terminal with yocto activated
```
bitbake fs-advantage
```

## Install OS Image ##
The OS Image can be installed to a SD card though the following command.  
```
sudo umount /dev/sdcX*
zcat tmp/deploy/images/advantage/fs-advantage-advantage.wic.gz | sudo dd of=/dev/sdX bs=1M && sync
```

In addition, all the output of the build including the Kernel Files, Bootloader and SD Card image are located at
```
fs_advantage/tmp/deploy/images/advantage/
```

License information may be found at
```
fs_advantage/tmp/deploy/licenses
```

## Build SDK ##
A SDK can be built from the image for cross compilation of applications outside of Yocto using the following command
```
bitbake fs-advantage -c populate_sdk
```
The SDK can be installed by running the script located at
```
tmp/deploy/sdk/fslc-framebuffer-glibc-x86_64-fs-advantage-armv7at2hf-neon-toolchain-2.5.sh
```