/*!
 * @file    labtester.dts
 * @author  Kevin Wijesekera <kwijesekera@munciepower.com>
 * @brief   This file contains the entry dts element for the LTS
 *
 * @attention
 *
 * Copyright (C) 2019 Muncie Power Products, Inc
 * Copyright 2016 Variscite, Ltd.
 * Copyright (C) 2013 Freescale Semiconductor, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/dts-v1/;

#include "imx6dl.dtsi"
#include "tester-core.dtsi"

/ {
	model = "Muncie LTS Hardware";
	compatible = "fsl,imx6dl-var-som", "fsl,imx6dl";

	gpio-poweroff {
		compatible = "gpio-poweroff";
		gpios = <&gpio5 18 GPIO_ACTIVE_HIGH>;
		timeout-ms = <3000>;
	};

};

&cpu0 {
	arm-supply = <&reg_arm>;
    soc-supply = <&reg_soc>;
};

&gpc {
	/* use ldo-enable, u-boot will check it and configure */
	fsl,ldo-bypass = <0>;
};

&snvs_poweroff {
    status = "disable";
};

&fec {
	/delete-property/ phy-reset-on-resume;
};

&reg_arm {
	/delete-property/ vin-supply;
};

&reg_pu {
	/delete-property/ vin-supply;
};

&reg_soc {
	/delete-property/ vin-supply;
};
