/*!
 * @file    ds90ub954_mipi.c
 * @author  Kevin Wijesekera <kwijesekera@munciepower.com>
 * @breif   This file contains a linux kernel driver for the DS90UB954 
 *          MIPI/I2C to FPD Link decoder
 *
 * @attention
 *
 * Copyright (C) 2019 Muncie Power Products, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/clk.h>
#include <linux/of_device.h>
#include <linux/i2c.h>
#include <linux/of_gpio.h>
#include <linux/pinctrl/consumer.h>
#include <linux/regulator/consumer.h>
#include <linux/fsl_devices.h>

#define DS90UB954_REG_RESET         0x01
#define DS90UB954_REG_DEV_STS       0x04
#define DS90UB954_REG_RX_PRT_CTR    0x0C
#define DS90UB954_REG_CSI_PLL_CTL   0x1F
#define DS90UB954_REG_FWD_CTL1      0x20
#define DS90UB954_REG_CSI_CTL       0x33
#define DS90UB954_REG_CSI_STS       0x35
#define DS90UB954_REG_CSI_TX_ISR    0x37
#define DS90UB954_REG_FPD3_PORT_SEL 0x4C
#define DS90UB954_REG_RX_PORT_STS1  0x4D
#define DS90UB954_REG_RX_PORT_STS2  0x4E
#define DS90UB954_REG_RX_FREQ_HIGH  0x4F
#define DS90UB954_REG_RX_FREQ_LOW   0x50
#define DS90UB954_REG_SENSOR_STS0   0x51
#define DS90UB954_REG_SENSOR_STS1   0x52
#define DS90UB954_REG_SENSOR_STS2   0x53
#define DS90UB954_REG_SENSOR_STS3   0x54
#define DS90UB954_REG_BCC_CONFIG    0x58
#define DS90UB954_REG_SER_ALIAS_ID  0x5C
#define DS90UB954_REG_SLAVEID0      0x5D
#define DS90UB954_REG_SLAVEALIAS0   0x65
#define DS90UB954_REG_BC_GPIO_CTL0  0x6E
#define DS90UB954_REG_BC_GPIO_CTL1  0x6F

static ssize_t ds90ub954_remote_reset(struct device* dev, struct device_attribute* attr, const char* buf, size_t count);
static ssize_t ds90ub954_status(struct device *dev, struct device_attribute *attr, char *buf);
static int ds90ub954_probe(struct i2c_client *adapter, const struct i2c_device_id *device_id);
static int ds90ub954_remove(struct i2c_client *client);

/*! DS90UB954 Data struct */
struct{
    struct i2c_client *i2c_client;
    bool ch1_en;
    bool ch2_en;
    int csi_lanes;
    bool lock;
    bool pass;
    int ch1_remote_reset_gpio;
    int ch2_remote_reset_gpio;
} ds90ub954_data;

/*! DS90UB954 I2C IDs struct */
static const struct i2c_device_id ds90ub954_id[] = {
	{"ds90ub954_mipi", 0},
	{},
};
MODULE_DEVICE_TABLE(i2c, ds90ub954_id);

/*! 
 * DS90UB954 SYSFS Device Nodes
 *
 * rmt_reset - write channel # to toggle remote reset of channel (GPIO based), 0220
 * fpd_stats - read status of channels, 0444
 */

static DEVICE_ATTR(rmt_reset, S_IWUSR | S_IWGRP, NULL, ds90ub954_remote_reset);
static DEVICE_ATTR(fpd_status,S_IRUGO, ds90ub954_status, NULL);

static struct attribute *dev_attrs[] = {
	&dev_attr_rmt_reset.attr,
	&dev_attr_fpd_status.attr,
	NULL,
};

static struct attribute_group dev_attr_group = {
	.attrs = dev_attrs,
};

static const struct attribute_group *dev_attr_groups[] = {
	&dev_attr_group,
	NULL,
};

/*! DS90UB954 I2C driver struct */
static struct i2c_driver ds90ub954_i2c_driver = {
	.driver = {
		  .owner = THIS_MODULE,
		  .name  = "ds90ub954_mipi",
          //.groups = dev_attr_groups,
		  },
	.probe  = ds90ub954_probe,
	.remove = ds90ub954_remove,
	.id_table = ds90ub954_id,
};


/*!
 * This function reads a register from the DS90UB954 I2C device
 *
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param reg device register to read
 *
 * @return 0 on success or an error code
 */
static inline int ds90ub954_read_reg(u8 reg)
{
	int val;

	val = i2c_smbus_read_byte_data(ds90ub954_data.i2c_client, reg);
	if (val < 0) {
		dev_info(&ds90ub954_data.i2c_client->dev,
			"%s:read reg error: reg=%2x\n", __func__, reg);
		return -EIO;
	}
	return val;
}

/*!
 * This function writes a register to the DS90UB954 I2C device
 *
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param reg device register to read
 * @param val value to write to register
 *
 * @return 0 on success or an error code
 */
static int ds90ub954_write_reg(u8 reg, u8 val)
{
	s32 ret;
    //dev_info(&ds90ub954_data.i2c_client->dev,"DS90UB954: Writing 0x%x = 0x%x\n",reg,val);
	ret = i2c_smbus_write_byte_data(ds90ub954_data.i2c_client, reg, val);
	if (ret < 0) {
		dev_info(&ds90ub954_data.i2c_client->dev,
			"%s:write reg error:reg=%2x,val=%2x\n", __func__,
			reg, val);
		return -EIO;
	}
	return 0;
}

/*!
 * (SYSFS): This is the handler for the rmt_reset SYSFS node write
 * It reads in a channel # and then toggles the remote reset GPIO
 * to reset any devices on that channel on the DS90UB954.
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/16/2019
 *
 * @param dev device* the sysfs hook was called with
 * @param attr device_attribute* the sysfs hook was called with
 * @param buf The buffer that was written to the sysfs node
 * @param count The size of the written buffer
 *
 * @return buffer size on success, error code on failure
 */
static ssize_t ds90ub954_remote_reset(struct device* dev, struct device_attribute* attr, const char* buf, size_t count){
    long channel;
    int reg1,reg2;

    //get channel number
	//Convert buffer to an integer
	if (kstrtol(buf, 10, &channel) != 0)
	{
		// incoming string didn't have valid data
        dev_warn(dev, "Invalid data passed to ds90ub954_remote_reset\n");
		return -EINVAL;
	}

    if(ds90ub954_data.ch1_en && channel == 1){
        //setup channel 1 read/write
        if(ds90ub954_write_reg(DS90UB954_REG_FPD3_PORT_SEL,0x01)){
            return -EIO;
        }

        //toggle reset GPIO
        reg1 = ds90ub954_read_reg(DS90UB954_REG_BC_GPIO_CTL0);
        reg2 = ds90ub954_read_reg(DS90UB954_REG_BC_GPIO_CTL1);

        if(ds90ub954_data.ch1_remote_reset_gpio > 1){
            reg2 &= ~(0xF << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4));
            reg2 |= 0x8 << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4);
        }
        else{   
            reg1 &= ~(0xF << (ds90ub954_data.ch1_remote_reset_gpio*4));
            reg1 |= 0x8 << (ds90ub954_data.ch1_remote_reset_gpio*4);
        }

        if(ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL0,reg1) || ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL1,reg2)){
            return -EIO;
        }

        //ds90ub954_data
        msleep(150);

        if(ds90ub954_data.ch1_remote_reset_gpio > 1){
            reg2 &= ~(0xF << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4));
            reg2 |= 0x9 << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4);
        }
        else{   
            reg1 &= ~(0xF << (ds90ub954_data.ch1_remote_reset_gpio*4));
            reg1 |= 0x9 << (ds90ub954_data.ch1_remote_reset_gpio*4);
        }

        if(ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL0,reg1) || ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL1,reg2)){
            return -EIO;
        }

        dev_info(dev,"Channel 1 remote reset triggered\n");
    }
    return count;
}

/*!
 * (SYSFS): This is the handler for the fpd_status SYSFS node read
 * It gets the data of the FPD Link bus and returns it as LOCK,PASS
 * of the DS90UB954.
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/18/2019
 *
 * @param dev device* the sysfs hook was called with
 * @param attr device_attribute* the sysfs hook was called with
 * @param buf The buffer that will be output by the read function
 *
 * @return length of the buffer to be output by the read function on success or error code on failure
 */
ssize_t ds90ub954_status(struct device *dev, struct device_attribute *attr, char *buf){
    int val;

    val = ds90ub954_read_reg(DS90UB954_REG_DEV_STS);
    if(!(val & 0x10)){
        dev_err(dev,"DEVICE_STS reports REFCLK not valid");
        return -EIO;
    }
    if(!(val & 0x80)){
        dev_err(dev,"DEVICE_STS reports CFG_CHECKSUM invalid");
        return -EIO;
    }
    if(!(val & 0x40)){
        dev_err(dev,"DEVICE_STS reports CFG_INIT not done");
        return -EIO;
    }
    ds90ub954_data.lock = (val & 0x4);
    ds90ub954_data.pass = (val & 0x8);

    return sprintf(buf,"%d,%d\n",ds90ub954_data.lock,ds90ub954_data.pass);
}

/*!
 * This is the I2C probe function for the DS90UB954 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/18/2019
 *
 * @param client i2c_client* that this device is bound to
 * @param id i2c_device_id* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static int ds90ub954_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
    struct device_node *np = client->dev.of_node;
	int val, i, reg, reg2, ret;

    memset(&ds90ub954_data, 0, sizeof(ds90ub954_data));

    ds90ub954_data.i2c_client = client;

    //get configuration - port en
    ds90ub954_data.ch1_en = of_get_property(np, "ch1-en", NULL) != NULL;
    ds90ub954_data.ch2_en = of_get_property(np, "ch2-en", NULL) != NULL;
    if(!ds90ub954_data.ch1_en && !ds90ub954_data.ch2_en){
        dev_err(dev,"No CSI channels enabled\n");
        return -EINVAL;
    }

    //get configuration csi lanes
    if(of_property_read_u32(np,"csi-lanes",&val) || (val != 1 && val != 2 && val != 4)){
        dev_err(dev,"Invalid csi-lanes(=%d)\n",val);
        return -EINVAL;
    }
    ds90ub954_data.csi_lanes = val;

    //setup RX_PORT_CTL
    if(ds90ub954_data.ch1_en && ds90ub954_data.ch2_en){
        reg = 0x3F;
        dev_err(dev,"Driver does not support dual channels\n");
        return -EINVAL;
    }
    else if(ds90ub954_data.ch2_en){
        reg = 0x16;
    }
    else{
        reg = 0x01;
    }
    if(ret =ds90ub954_write_reg(DS90UB954_REG_RX_PRT_CTR,reg)){
        return ret;
    }

    //get csi-speed
    if(of_property_read_u32(np,"csi-speed",&val) || (val != 400 && val != 800 && val != 1600)){
        val = 400;
        dev_warn(dev, "Invalid csi-speed(=%d)Mbps, fall back to 400Mbps\n",val);
    }

    //setup CSI_PLL_CTL 
    if(val == 1600){
        reg = 0;
    }   
    if(val == 800){
        reg = 0x2;
    }
    else{
        reg = 0x3;
    }
    if(ret = ds90ub954_write_reg(DS90UB954_REG_CSI_PLL_CTL,reg)){
        return ret;
    }

    //setup FWD_CTL1
    if(ds90ub954_data.ch1_en && ds90ub954_data.ch2_en){
        reg = 0x0;
    }
    else if(ds90ub954_data.ch2_en){
        reg = 0x10;
    }
    else{
        reg = 0x20;
    }
    if(ret = ds90ub954_write_reg(DS90UB954_REG_FWD_CTL1,reg)){
        return ret;
    }
    //Setup REG_CSI_CTL
    reg = 0x1;
    if(ds90ub954_data.csi_lanes == 1){
        reg |= 0x30;
    }
    if(ds90ub954_data.csi_lanes == 2){
        reg |= 0x20;
    }
    else if(ds90ub954_data.csi_lanes == 4){
        reg |= 0x10;
    }
    if(ret = ds90ub954_write_reg(DS90UB954_REG_CSI_CTL,reg)){
        return ret;
    }
    //setup channel 1
    if(ds90ub954_data.ch1_en){

        //get configuration - port reset gpio
        if(of_property_read_u32(np,"ch1-rmtreset",&val) || ds90ub954_data.ch1_remote_reset_gpio > 3){
            val = 0;
            dev_warn(dev, "No ch1-rmtreset, fall back to GPIO 0\n");
        }
        ds90ub954_data.ch1_remote_reset_gpio = val;

        //setup channel 1 read/write
        if(ret = ds90ub954_write_reg(DS90UB954_REG_FPD3_PORT_SEL,0x01)){
            return ret;
        }
        //get ch1-freq
        if(of_property_read_u32(np,"ch1-freq",&val)){
            val = 50;
            dev_warn(dev, "No ch1-freq, fall back to 50Mbps\n");
        }

        //Setup BCC_CONFIG
        reg = 0x58; // i2c pass though, bc_always on, bc crc, 2 MBps back channel
        if(val == 100){
            reg |= 0x7;
        }
        else if(val == 50){
            reg |= 0x6;
        }
        else if(val == 25){
            reg |= 0x5;
        }
        else if(val == 10){
            reg |= 0x4;
        }
        else if(val != 2){
            reg |= 0x6;
            dev_warn(dev, "Invalid ch1-freq(=%d)Mbps, fall back to 50Mbps\n", val);
        }
        if(ret = ds90ub954_write_reg(DS90UB954_REG_BCC_CONFIG,reg)){
            return ret;
        }
        //get ch1-rmtaddr
        if(of_property_read_u32(np,"ch1-seraddr",&val)){
            dev_err(dev,"No ch1-seraddr supplied\n");
            return -EINVAL;
        }

        //setup SER_ALIAS_ID
        if(ret = ds90ub954_write_reg(DS90UB954_REG_SER_ALIAS_ID,val << 1)){
            return ret;
        }

        //get ch1-map, set SLAVEID0 - SLAVEID7 and SLAVEALIAS0 - SLAVEALIAS7
        for(i=0;i<16;i+=2){
            if(!of_property_read_u32_index(np,"ch1-map",i,&reg) && !of_property_read_u32_index(np,"ch1-map",i+1,&reg2)){
                if(ret = ds90ub954_write_reg(DS90UB954_REG_SLAVEID0 + (i/2),reg << 1)){
                    return ret;
                }
                if(ret = ds90ub954_write_reg(DS90UB954_REG_SLAVEALIAS0 + (i/2),reg2 << 1)){
                    return ret;
                }
            }
        }

        //read port status
        ds90ub954_read_reg(DS90UB954_REG_RX_PORT_STS1);
        ds90ub954_read_reg(DS90UB954_REG_RX_PORT_STS2);
    }
    //setup channel 2
    else if(ds90ub954_data.ch2_en){

    
        //get configuration - port reset gpio
        if(of_property_read_u32(np,"ch2-rmtreset",&val) || ds90ub954_data.ch2_remote_reset_gpio > 3){
            val = 0;
            dev_warn(dev, "No ch2-rmtreset, fall back to GPIO 0\n");
        }
        ds90ub954_data.ch2_remote_reset_gpio = val;

        //setup channel 2 read/write
        if(ret = ds90ub954_write_reg(DS90UB954_REG_FPD3_PORT_SEL,0x12)){
            return ret;
        }
        //get ch2-freq
        if(of_property_read_u32(np,"ch2-freq",&val)){
            val = 50;
            dev_warn(dev, "No ch2-freq, fall back to 50Mbps\n");
        }

        //Setup BCC_CONFIG
        reg = 0x58; // i2c pass though, bc_always on, bc crc, 2 MBps back channel
        if(val == 100){
            reg |= 0x7;
        }
        else if(val == 50){
            reg |= 0x6;
        }
        else if(val == 25){
            reg |= 0x5;
        }
        else if(val == 10){
            reg |= 0x4;
        }
        else if(val != 2){
            reg |= 0x6;
            dev_warn(dev, "Invalid ch1-freq(=%d)Mbps, fall back to 50Mbps\n", val);
        }
        if(ret = ds90ub954_write_reg(DS90UB954_REG_BCC_CONFIG,reg)){
            return ret;
        }
        //get ch2-rmtaddr
        if(of_property_read_u32(np,"ch2-seraddr",&val)){
            dev_err(dev,"No ch2-seraddr supplied\n");
            return -EINVAL;
        }

        //setup SER_ALIAS_ID
        if(ret = ds90ub954_write_reg(DS90UB954_REG_SER_ALIAS_ID,val << 1)){
            return ret;
        }

        //get ch2-map, set SLAVEID0 - SLAVEID7 and SLAVEALIAS0 - SLAVEALIAS7
        for(i=0;i<16;i+=2){
            if(!of_property_read_u32_index(np,"ch2-map",i,&reg) && !of_property_read_u32_index(np,"ch2-map",i+1,&reg2)){
                if(ret = ds90ub954_write_reg(DS90UB954_REG_SLAVEID0 + (i/2),reg << 1)){
                    return ret;
                }
                if(ret = ds90ub954_write_reg(DS90UB954_REG_SLAVEALIAS0 + (i/2),reg2 << 1)){
                    return ret;
                }
            }
        }

        reg = 0x88;
        reg2 = 0x88;
        //toggle reset GPIO
        if(ds90ub954_data.ch1_remote_reset_gpio > 1){
            reg2 &= ~(0xF << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4));
            reg2 |= 0x9 << ((ds90ub954_data.ch1_remote_reset_gpio-2)*4);
        }
        else{
            reg &= ~(0xF << (ds90ub954_data.ch1_remote_reset_gpio*4));
            reg |= 0x9 << (ds90ub954_data.ch1_remote_reset_gpio*4);
        }

        if(ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL0,reg) || ds90ub954_write_reg(DS90UB954_REG_BC_GPIO_CTL1,reg2)){
            return -EIO;
        }

        //read port status
        ds90ub954_read_reg(DS90UB954_REG_RX_PORT_STS1);
        ds90ub954_read_reg(DS90UB954_REG_RX_PORT_STS2);
    }


    //read status
    ds90ub954_read_reg(DS90UB954_REG_CSI_STS);
    ds90ub954_read_reg(DS90UB954_REG_CSI_TX_ISR);

    //wait for lock
    msleep(100);

    //check DEVICE_STS
    val = ds90ub954_read_reg(DS90UB954_REG_DEV_STS);
    if(!(val & 0x10)){
        dev_err(dev,"DEVICE_STS reports REFCLK not valid");
        return -EIO;
    }
    if(!(val & 0x80)){
        dev_err(dev,"DEVICE_STS reports CFG_CHECKSUM invalid");
        return -EIO;
    }
    if(!(val & 0x40)){
        dev_err(dev,"DEVICE_STS reports CFG_INIT not done");
        return -EIO;
    }
    ds90ub954_data.lock = (val & 0x4);
    ds90ub954_data.pass = (val & 0x8);
    if(!ds90ub954_data.lock || !ds90ub954_data.pass){
        dev_warn(dev,"DEVICE_STS reports no lock(%d)/pass(%d)",ds90ub954_data.lock,ds90ub954_data.pass);
    }

    //add device nodes
    if(ret = sysfs_create_group(&dev->kobj,&dev_attr_group)){
        return ret;
    }


    if(ds90ub954_data.ch1_en){
        //setup channel 1 read/write
        if(ret = ds90ub954_write_reg(DS90UB954_REG_FPD3_PORT_SEL,0x01)){
            return ret;
        }
        //get FPD Link Freq
        val = ds90ub954_read_reg(DS90UB954_REG_RX_FREQ_HIGH) * 1000;
        val += (ds90ub954_read_reg(DS90UB954_REG_RX_FREQ_LOW) * 1000) / 256;
        dev_info(dev,"FPD Link Running on CH1 = %dkHz\n",val);
    }
    else if(ds90ub954_data.ch2_en){
        //setup channel 2 read/write
        if(ret = ds90ub954_write_reg(DS90UB954_REG_FPD3_PORT_SEL,0x12)){
            return ret;
        }
        //get FPD Link Freq
        val = ds90ub954_read_reg(DS90UB954_REG_RX_FREQ_HIGH) * 1000;
        val += (ds90ub954_read_reg(DS90UB954_REG_RX_FREQ_LOW) * 1000) / 256;
        dev_info(dev,"FPD Link Running on CH2 = %dkHz\n",val);
    }
	return 0;
}

/*!
 * This is the I2C detach function for the DS90UB954 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/16/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static int ds90ub954_remove(struct i2c_client *client)
{
    sysfs_remove_group(&client->dev.kobj,&dev_attr_group);
	return 0;
}

/*!
 * This is the driver init function for the DS90UB954 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static __init int ds90ub954_init(void)
{
	u8 err;

	err = i2c_add_driver(&ds90ub954_i2c_driver);
	if (err != 0)
		pr_err("%s:driver registration failed, error=%d\n",
			__func__, err);

	return err;
}

/*!
 * This is the driver cleanup function for the DS90UB954 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static void __exit ds90ub954_clean(void)
{
	i2c_del_driver(&ds90ub954_i2c_driver);
}

module_init(ds90ub954_init);
module_exit(ds90ub954_clean);

MODULE_AUTHOR("Muncie Power Products, Inc");
MODULE_DESCRIPTION("DS90UB954 MIPI Bridge Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_ALIAS("FPDRX");
