/*!
 * @file    ds90ub953_mipi.c
 * @author  Kevin Wijesekera <kwijesekera@munciepower.com>
 * @breif   This file contains a linux kernel driver for the DS90UB9543
 *          MIPI/I2C to FPD Link encoder
 *
 * @attention
 *
 * Copyright (C) 2019 Muncie Power Products, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/clk.h>
#include <linux/of_device.h>
#include <linux/i2c.h>
#include <linux/of_gpio.h>
#include <linux/pinctrl/consumer.h>
#include <linux/regulator/consumer.h>
#include <linux/fsl_devices.h>

#define DS90UB953_REG_GEN_CFG           0x02
#define DS90UB953_REG_CLKOUT_CTRL0      0x06
#define DS90UB953_REG_CLKOUT_CTRL1      0x07
#define DS90UB953_REG_LOCAL_GPIO_DATA   0x0D
#define DS90UB953_REG_GPIO_INPUT_CTRL   0x0E
#define DS90UB953_REG_DEVICE_STS        0x51
#define DS90UB953_REG_GENERAL_STATUS    0x52

static int ds90ub953_probe(struct i2c_client *adapter, const struct i2c_device_id *device_id);
static int ds90ub953_remove(struct i2c_client *client);

/*! DS90UB953 Data struct */
struct{
    struct i2c_client *i2c_client;
    int csi_lanes;
} ds90ub953_data;

/*! DS90UB953 I2C IDs struct */
static const struct i2c_device_id ds90ub953_id[] = {
	{"ds90ub953_mipi", 0},
	{},
};
MODULE_DEVICE_TABLE(i2c, ds90ub953_id);

/*! DS90UB953 I2C driver struct */
static struct i2c_driver ds90ub953_i2c_driver = {
	.driver = {
		  .owner = THIS_MODULE,
		  .name  = "ds90ub953_mipi",
		  },
	.probe  = ds90ub953_probe,
	.remove = ds90ub953_remove,
	.id_table = ds90ub953_id,
};


/*!
 * This function reads a register from the DS90UB953 I2C device
 *
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param reg device register to read
 *
 * @return 0 on success or an error code
 */
static inline int ds90ub953_read_reg(u8 reg)
{
	int val;

	val = i2c_smbus_read_byte_data(ds90ub953_data.i2c_client, reg);
	if (val < 0) {
		dev_info(&ds90ub953_data.i2c_client->dev,
			"%s:read reg error: reg=%2x\n", __func__, reg);
		return -EIO;
	}
	return val;
}

/*!
 * This function writes a register to the DS90UB953 I2C device
 *
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param reg device register to read
 * @param val value to write to register
 *
 * @return 0 on success or an error code
 */
static int ds90ub953_write_reg(u8 reg, u8 val)
{
	s32 ret;
    //dev_info(&ds90ub953_data.i2c_client->dev,"ds90ub953: Writing 0x%x = 0x%x\n",reg,val);
	ret = i2c_smbus_write_byte_data(ds90ub953_data.i2c_client, reg, val);
	if (ret < 0) {
		dev_info(&ds90ub953_data.i2c_client->dev,
			"%s:write reg error:reg=%2x,val=%2x\n", __func__,
			reg, val);
		return -EIO;
	}
	return 0;
}

/*!
 * This is the I2C probe function for the DS90UB953 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/16/2019
 *
 * @param client i2c_client* that this device is bound to
 * @param id i2c_device_id* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static int ds90ub953_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
    struct device_node *np = client->dev.of_node;
	int val, i, ret;
    bool valb;
    unsigned char reg;

    memset(&ds90ub953_data, 0, sizeof(ds90ub953_data));

    ds90ub953_data.i2c_client = client;


    //get configuration csi lanes
    if(of_property_read_u32(np,"csi-lanes",&val) || (val != 1 && val != 2 && val != 4)){
        dev_err(dev,"Invalid csi-lanes(=%d)\n",val);
        return -EINVAL;
    }
    ds90ub953_data.csi_lanes = val;

    //set REG_GEN_CFG
    //get io-v18 and set reg flag for 1.8 volt or 3.3volt io
    valb = of_get_property(np, "io-v18", NULL) != NULL;
    reg = valb?0x1:0;

    reg |= 0x2; //crc tx gen

    if(ds90ub953_data.csi_lanes == 1){
        reg |= 0;
    }
    if(ds90ub953_data.csi_lanes == 2){
        reg |= 0x10;
    }
    else if(ds90ub953_data.csi_lanes == 4){
        reg |= 0x30;
    }
    if(ret = ds90ub953_write_reg(DS90UB953_REG_GEN_CFG,reg)){
        return ret;
    }

    //get HS_CLK_DIV
    if(of_property_read_u32_index(np,"clock-output",0,&val) || val > 4){
        dev_err(dev,"Invalid clock-output:0 DIV(=%d)\n",val);
        return -EINVAL;
    }
    reg = val << 5;

    //get M_VAL
    if(of_property_read_u32_index(np,"clock-output",1,&val) || val > 0x1F || val == 0){
        dev_err(dev,"Invalid clock-output:1 M(=%d)\n",val);
        return -EINVAL;
    }
    reg |= val;
    
    //write REG_CLKOUT_CTRL0
    if(ret = ds90ub953_write_reg(DS90UB953_REG_CLKOUT_CTRL0,reg)){
        return ret;
    }

    //get N_VAL
    if(of_property_read_u32_index(np,"clock-output",2,&val) || val == 0){
        dev_err(dev,"Invalid clock-output:2 N(=%d)\n",val);
        return -EINVAL;
    }

    //write REG_CLOCKOUT_CTRL1
    if(ret = ds90ub953_write_reg(DS90UB953_REG_CLKOUT_CTRL1,val)){
        return ret;
    }

    //write LOCAL_GPIO_DATA - Enable remote GPIO's
    if(ret = ds90ub953_write_reg(DS90UB953_REG_LOCAL_GPIO_DATA,0xF0)){
        return ret;
    }

    //get gpio-cfg
    reg = 0x0F;
    for(i=0;i<4;i++){
        if(!of_property_read_u32_index(np,"gpio-cfg",i,&val)){
            if(val == 0){
                reg |= 1 << (i+4);
                reg &= ~(1 << i);
            }
        }
    }

    //write GPIO_INPUT_CTRL
    if(ret = ds90ub953_write_reg(DS90UB953_REG_GPIO_INPUT_CTRL,reg)){
        return ret;
    }

    //check REG_DEVICE_STS
    val = ds90ub953_read_reg(DS90UB953_REG_DEVICE_STS);
    if(!(val & 0x80)){
        dev_err(dev,"device CFG_CHECKSUM invalid, device_sts=(0x%x)",val);
        return -1;
    }
    if(!(val & 0x40)){
        dev_err(dev,"device CFG_INIT not done, device_sts=(0x%x)",val);
        return -1;
    }

    //check REG_GENERAL_STATUS
    val = ds90ub953_read_reg(DS90UB953_REG_GENERAL_STATUS);
    if(!(val & 0x01)){
        dev_err(dev,"device back channel link not established, general_status=(0x%x)",val);
        return -1;
    }
    if(!(val & 0x40)){
        dev_err(dev,"device LOCK not detected, general_status=(0x%x)",val);
        return -1;
    }

    dev_info(dev,"back channel connection established");

	return 0;
}

/*!
 * This is the I2C detach function for the DS90UB953 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static int ds90ub953_remove(struct i2c_client *client)
{

	return 0;
}

/*!
 * This is the driver init function for the DS90UB953 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static __init int ds90ub953_init(void)
{
	u8 err;

	err = i2c_add_driver(&ds90ub953_i2c_driver);
	if (err != 0)
		pr_err("%s:driver registration failed, error=%d\n",
			__func__, err);

	return err;
}

/*!
 * This is the driver cleanup function for the DS90UB953 device
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @date 4/11/2019
 *
 * @param client i2c_client* that this device is bound to
 *
 * @return 0 on sucess or error code on failure
 */
static void __exit ds90ub953_clean(void)
{
	i2c_del_driver(&ds90ub953_i2c_driver);
}

module_init(ds90ub953_init);
module_exit(ds90ub953_clean);

MODULE_AUTHOR("Muncie Power Products, Inc");
MODULE_DESCRIPTION("DS90UB953 MIPI Bridge Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_ALIAS("FPDTX");
