/*!
 * @file    adv_wdt.c
 * @author  Kevin Wijesekera <kwijesekera@munciepower.com>
 * @breif   This file contains a linux kernel driver for the ADV
            GPIO/LED watchdog timer
 *
 * @attention
 *
 * Copyright (C) 2019 Muncie Power Products, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <linux/err.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/platform_device.h>
#include <linux/watchdog.h>

#define SOFT_TIMEOUT_MIN	1
#define SOFT_TIMEOUT_DEF	60
#define SOFT_TIMEOUT_MAX	0xffff

struct adv_wdt_priv {
    int gpio_wdt;
    int gpio_led;
    bool gpio_wdt_init;
    bool gpio_led_init;
    bool gpio_wdt_state;
    bool gpio_led_state;
    bool armed;
    unsigned int hw_margin;
    unsigned int last_jiffies;
    struct timer_list timer;
    struct watchdog_device wdd;
};

static void adv_wdt_hwping(unsigned long data)
{
	struct watchdog_device *wdd = (struct watchdog_device *)data;
	struct adv_wdt_priv *priv = watchdog_get_drvdata(wdd);

    if(priv->armed){

        if(time_after(jiffies, priv->last_jiffies + msecs_to_jiffies(wdd->timeout * 1000))) {
            dev_crit(wdd->parent, "Timer expired. System will reboot soon!\n");
            return;
        }

        priv->gpio_led_state = !priv->gpio_led_state;
        gpio_set_value_cansleep(priv->gpio_led, priv->gpio_led_state);
    }

    

	/* Restart timer */
	mod_timer(&priv->timer, jiffies + priv->hw_margin);

    priv->gpio_wdt_state = !priv->gpio_wdt_state;
    gpio_set_value_cansleep(priv->gpio_wdt, priv->gpio_wdt_state);
}


static int adv_wdt_start(struct watchdog_device *wdd)
{
	struct adv_wdt_priv *priv = watchdog_get_drvdata(wdd);

    priv->gpio_wdt_state = !priv->gpio_wdt_init;
    priv->gpio_led_state = !priv->gpio_led_init;

    gpio_direction_output(priv->gpio_wdt, priv->gpio_wdt_state);
    gpio_direction_output(priv->gpio_led, priv->gpio_led_state);

	priv->last_jiffies = jiffies;
	adv_wdt_hwping((unsigned long)&priv->wdd);
	priv->armed = true;

	return 0;
}

static int adv_wdt_stop(struct watchdog_device *wdd)
{
	struct adv_wdt_priv *priv = watchdog_get_drvdata(wdd);

	priv->armed = false;
    mod_timer(&priv->timer, 0);

    gpio_set_value_cansleep(priv->gpio_wdt, priv->gpio_wdt_init);
    gpio_set_value_cansleep(priv->gpio_led, priv->gpio_led_init);

	return 0;
}

static int adv_wdt_ping(struct watchdog_device *wdd)
{
	struct adv_wdt_priv *priv = watchdog_get_drvdata(wdd);

	priv->last_jiffies = jiffies;

	return 0;
}

static int adv_wdt_set_timeout(struct watchdog_device *wdd, unsigned int t)
{
	wdd->timeout = t;

	return adv_wdt_ping(wdd);
}

static const struct watchdog_info adv_wdt_ident = {
	.options	= WDIOF_MAGICCLOSE | WDIOF_KEEPALIVEPING |
			  WDIOF_SETTIMEOUT,
	.identity	= "ADV Watchdog",
};

static const struct watchdog_ops adv_wdt_ops = {
	.owner		= THIS_MODULE,
	.start		= adv_wdt_start,
	.stop		= adv_wdt_stop,
	.ping		= adv_wdt_ping,
	.set_timeout	= adv_wdt_set_timeout,
};

static int adv_wdt_probe(struct platform_device *pdev)
{
	struct adv_wdt_priv *priv;
	enum of_gpio_flags flags;
	unsigned int val;
	int ret;

    //setup struct
	priv = devm_kzalloc(&pdev->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	platform_set_drvdata(pdev, priv);

    //check/reset WDT GPIO
	priv->gpio_wdt = of_get_gpio_flags(pdev->dev.of_node, 0, &flags);
	if (!gpio_is_valid(priv->gpio_wdt)){
        dev_err(&pdev->dev,"WDT Main GPIO invalid\n");
        return priv->gpio_wdt;
    }
		

    priv->gpio_wdt_init = (flags & OF_GPIO_ACTIVE_LOW)? GPIOF_OUT_INIT_HIGH : GPIOF_OUT_INIT_LOW;
    ret = devm_gpio_request_one(&pdev->dev, priv->gpio_wdt, priv->gpio_wdt_init, dev_name(&pdev->dev));
	if (ret){
        dev_err(&pdev->dev,"WDT Main GPIO invalid configuration\n");
        return ret;
    }

	priv->gpio_led = of_get_gpio_flags(pdev->dev.of_node, 1, &flags);
	if (!gpio_is_valid(priv->gpio_led)){
        dev_err(&pdev->dev,"WDT LED GPIO invalid\n");
        return priv->gpio_led;
    }
		

    priv->gpio_led_init = (flags & OF_GPIO_ACTIVE_LOW)? GPIOF_OUT_INIT_HIGH : GPIOF_OUT_INIT_LOW;
    ret = devm_gpio_request_one(&pdev->dev, priv->gpio_led, priv->gpio_led_init, dev_name(&pdev->dev));
	if (ret){
        dev_err(&pdev->dev,"WDT LED GPIO invalid configuration\n");
        return ret;
    }

	ret = of_property_read_u32(pdev->dev.of_node,"hw_margin_ms", &val);

	if (ret)
		return ret;
	/* Disallow values lower than 2 and higher than 65535 ms */
	if (val < 2 || val > 65535)
		return -EINVAL;

	/* Use safe value (1/2 of real timeout) */
	priv->hw_margin = msecs_to_jiffies(val / 2);

	watchdog_set_drvdata(&priv->wdd, priv);

	priv->wdd.info		= &adv_wdt_ident;
	priv->wdd.ops		= &adv_wdt_ops;
	priv->wdd.min_timeout	= SOFT_TIMEOUT_MIN;
	priv->wdd.max_timeout	= SOFT_TIMEOUT_MAX;
	priv->wdd.parent	= &pdev->dev;

	if (watchdog_init_timeout(&priv->wdd, 0, &pdev->dev) < 0)
		priv->wdd.timeout = SOFT_TIMEOUT_DEF;

	setup_timer(&priv->timer, adv_wdt_hwping, (unsigned long)&priv->wdd);

	watchdog_stop_on_reboot(&priv->wdd);

	ret = watchdog_register_device(&priv->wdd);
	if (ret)
		return ret;

	return 0;
}

static int adv_wdt_remove(struct platform_device *pdev)
{
	struct adv_wdt_priv *priv = platform_get_drvdata(pdev);

	del_timer_sync(&priv->timer);
	watchdog_unregister_device(&priv->wdd);

	return 0;
}

static const struct of_device_id adv_wdt_dt_ids[] = {
	{ .compatible = "mpp,adv-gpio", },
	{ }
};
MODULE_DEVICE_TABLE(of, adv_wdt_dt_ids);

static struct platform_driver adv_wdt_driver = {
	.driver	= {
		.name		= "adv-wdt",
		.of_match_table	= adv_wdt_dt_ids,
	},
	.probe	= adv_wdt_probe,
	.remove	= adv_wdt_remove,
};

module_platform_driver(adv_wdt_driver);

MODULE_AUTHOR("Kevin Wijesekera <kwijesekera@munciepower.com");
MODULE_DESCRIPTION("ADV Watchdog");
MODULE_LICENSE("GPL");
