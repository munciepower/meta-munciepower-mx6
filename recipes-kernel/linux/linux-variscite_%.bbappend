
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

LOCALVERSION_advantage = "-mx6"

SRC_URI += "file://adv.dts \
            file://adv-prog.dts \
            file://advq.dts \
            file://advq-prog.dts \
            file://adv-display.dtsi \
            file://adv-camera.dtsi \
            file://adv-core.dtsi \
            file://tester.dts \
            file://labtester.dts \
            file://labtesterq.dts \
            file://tester-core.dtsi \
            file://00-Ethernet-dts-flag-to-disable-gbit.patch \
            file://01-IPU-update-capture-driver-to-support-up-to-four-ch.patch \
            file://02-Remove-the-page-size-align-requirement-v4l2.patch \
            file://03-IPU-add-MEM-VDI-MEM-and-IC-memory-copy-support.patch \
            file://04-Add-ISL7998x-MIPI-Video-Decoder-Driver.patch \
            file://05-Add-DS90UB-Drivers.patch \
            file://06-Add-adv_wdt.patch \
            file://isl7998x_mipi.c \
            file://ds90ub954_mipi.c \
            file://ds90ub953_mipi.c \
            file://adv_wdt.c \
            file://logo_linux_clut224.ppm"

do_configure_prepend_muncieuts () {
    kernel_conf_variable USB_PRINTER m
    kernel_conf_variable RTC_DRV_BQ32K y
}


do_configure_prepend () {
    cp ${WORKDIR}/logo_linux_clut224.ppm ${S}/drivers/video/logo/

    #General
    kernel_conf_variable POWER_RESET_GPIO y
    #kernel_conf_variable RD_GZIP n

    #Power Management options
    #kernel_conf_variable SUSPEND n

    #Networking -> Networking options
    kernel_conf_variable IPV6 n

    #Networking -> Network Options -> Network packet filtering -> Core Netfilter
    kernel_conf_variable NETFILTER_XT_MATCH_STATE m

    #Drivers -> Misc
    kernel_conf_variable SENSORS_FXOS8700 n
    kernel_conf_variable SENSORS_FXAS2100X n

    #Drivers -> Misc -> EEPROM
    kernel_conf_variable EEPROM_AT24 n
    kernel_conf_variable EEPROM_AT25 n
    kernel_conf_variable EEPROM_93CX6 n

    #Drivers -> SCSI
    kernel_conf_variable BLK_DEV_SD y
    kernel_conf_variable SCSI_CONSTANTS n
    kernel_conf_variable SCSI_LOGGING y
    kernel_conf_variable SCSI_SCAN_ASYNC n

    #Drivers -> Serial ATA
    kernel_conf_variable ATA n
    kernel_conf_variable ATA_VERBOSE_ERROR n
    kernel_conf_variable SATA_PMP n
    kernel_conf_variable SATA_AHCI_PLATFORM n
    kernel_conf_variable AHCI_IMX n

    #Drivers -> Networking
    kernel_conf_variable TUN n
    kernel_conf_variable PPP n

    #Drivers -> Networking -> PHY
    kernel_conf_variable MICROCHIP_PHY n

    #Drivers -> Networking -> USB
    kernel_conf_variable USB_CATC n
    kernel_conf_variable USB_KAWETH n
    kernel_conf_variable USB_PEGASUS n
    kernel_conf_variable USB_RTL8150 n
    kernel_conf_variable USB_RTL8152 n
    kernel_conf_variable USB_LAN78XX n
    kernel_conf_variable USB_USBNET y
    kernel_conf_variable USB_NET_AX8817X m
    kernel_conf_variable USB_NET_AX88179_178A n
    kernel_conf_variable USB_NET_CDCETHER y
    kernel_conf_variable USB_NET_CDC_EEM y
    kernel_conf_variable USB_NET_CDC_NCM m
    kernel_conf_variable USB_NET_HUAWEI_CDC_NCM n
    kernel_conf_variable USB_NET_CDC_MBIM n
    kernel_conf_variable USB_NET_DM9601 n
    kernel_conf_variable USB_NET_SR9700 n
    kernel_conf_variable USB_NET_SR9800 n
    kernel_conf_variable USB_NET_SMSC75XX n
    kernel_conf_variable USB_NET_SMSC95XX n
    kernel_conf_variable USB_NET_GL620A n
    kernel_conf_variable USB_NET_NET1080 n
    kernel_conf_variable USB_NET_PLUSB n
    kernel_conf_variable USB_NET_MCS7830 n
    kernel_conf_variable USB_NET_RNDIS_HOST n
    kernel_conf_variable USB_NET_CDC_SUBSET y
    kernel_conf_variable USB_BELKIN n
    kernel_conf_variable USB_NET_ZAURUS n

    #Drivers -> Networking -> Wireless
    kernel_conf_variable WLAN_VENDOR_BROADCOM n
    kernel_conf_variable WLAN_VENDOR_MEDIATEK n
    kernel_conf_variable WLAN_VENDOR_RALINK n
    kernel_conf_variable WLAN_VENDOR_REALTEK n
    kernel_conf_variable WLAN_VENDOR_ZYDAS n

    #Drivers -> Input
    kernel_conf_variable TOUCHSCREEN_FTS n
    kernel_conf_variable INPUT_MISC n
    kernel_conf_variable INPUT_MOUSE n
    #kernel_conf_variable INPUT_KEYBOARD n

    #Drivers -> Input -> Touchscreens
    kernel_conf_variable TOUCHSCREEN_ADS7846 n
    kernel_conf_variable TOUCHSCREEN_EGALAX n
    kernel_conf_variable TOUCHSCREEN_ELAN_TS n
    kernel_conf_variable TOUCHSCREEN_GOODIX y
    kernel_conf_variable TOUCHSCREEN_CT36X_WLD n
    kernel_conf_variable TOUCHSCREEN_MAX11801 n
    kernel_conf_variable TOUCHSCREEN_IMX6UL_TSC n
    kernel_conf_variable TOUCHSCREEN_EDT_FT5X06 n
    kernel_conf_variable TOUCHSCREEN_TSC2007 n
    kernel_conf_variable TOUCHSCREEN_STMPE n
    kernel_conf_variable TOUCHSCREEN_SYNAPTICS_DSX n
    kernel_conf_variable TOUCHSCREEN_MC13783 n

    #Drivers -> Input -> Misc
    kernel_conf_variable INPUT_MMA8450 n
    kernel_conf_variable INPUT_MPL3115 n
    kernel_conf_variable SENSOR_FXLS8471 n
    kernel_conf_variable INPUT_ISL29023 n

    #Drivers -> SPI
    kernel_conf_variable SPI_GPIO n
    #kernel_conf_variable SPI_SPIDEV n

    #Drivers -> GPIO -> I2C Expanders
    kernel_conf_variable GPIO_MAX732X n
    kernel_conf_variable GPIO_PCA953X n

    #Drivers -> GPIO -> SPI Expanders
    kernel_conf_variable GPIO_74X164 n

    #Drivers -> Hardware Mon
    kernel_conf_variable MXC_MMA8451 n
    kernel_conf_variable SENSORS_MAG3110 n

    #Drivers -> Watchdog
    kernel_conf_variable MPP_ADV_WATCHDOG y
    kernel_conf_variable IMX2_WDT n

    #drivers -> Graphics -> Backlight & LCD
    kernel_conf_variable LCD_L4F00242T03 n
    kernel_conf_variable BACKLIGHT_GENERIC n
    kernel_conf_variable BACKLIGHT_GPIO n
    kernel_conf_variable FRAMEBUFFER_CONSOLE n

    #Drivers -> Graphics -> Bootup logo
    kernel_conf_variable LOGO y
    kernel_conf_variable LOGO_LINUX_CLUT224 y
    kernel_conf_variable LOGO_VARISCITE_CLUT224 n

    #Drivers -> Multimedia
    kernel_conf_variable MEDIA_RADIO_SUPPORT n
    kernel_conf_variable MEDIA_RC_SUPPORT n
    kernel_conf_variable MEDIA_CONTROLLER y
    kernel_conf_variable VIDEO_V4L2_SUBDEV_API y
    
    #Drivers -> Multimedia -> V4L Platform
    kernel_conf_variable VIDEO_MXC_OUTPUT y
    kernel_conf_variable VIDEO_MXC_CAPTURE y
    kernel_conf_variable VIDEO_MXC_CSI_CAMERA y
    kernel_conf_variable MXC_VADC y
    kernel_conf_variable MXC_MIPI_CSI y

    #Drivers -> Multimedia -> V4L Platform -> MXC Camera
    kernel_conf_variable VIDEO_MXC_IPU_CAMERA y
    kernel_conf_variable MXC_CAMERA_OV5640 n
    kernel_conf_variable MXC_CAMERA_OV5640_V2 n
    kernel_conf_variable MXC_CAMERA_OV5642 n
    kernel_conf_variable MXC_CAMERA_OV5640_MIPI n
    kernel_conf_variable MXC_CAMERA_OV5640_MIPI_V2 n
    kernel_conf_variable MXC_CAMERA_OV5647_MIPI n
    kernel_conf_variable MXC_TVIN_ADV7180 n
    kernel_conf_variable MXC_IPU_DEVICE_QUEUE_SDC y
    kernel_conf_variable MXC_IPU_PRP_ENC y
    kernel_conf_variable IPU_CSI_ENC n
    kernel_conf_variable MXC_IPU_CSI_ENC y
    kernel_conf_variable VIDEO_V4L2_MXC_INT_DEVICE y
    kernel_conf_variable MXC_TVIN_ISL7998X_MIPI m
    kernel_conf_variable MXC_DS90UB954_MIPI m
    kernel_conf_variable MXC_DS90UB953_MIPI m

    #Drivers -> MXC Support
    kernel_conf_variable MXC_HDMI_CEC n

    #Drivers -> USB -> USB Serial Converter
    kernel_conf_variable USB_SERIAL_PL2303 m

    #Drivers -> LED Support -> LED Trigger support
    kernel_conf_variable LEDS_TRIGGER_ONESHOT n
    kernel_conf_variable LEDS_TRIGGER_HEARTBEAT n
    kernel_conf_variable LEDS_TRIGGER_BACKLIGHT n
    kernel_conf_variable LEDS_TRIGGER_GPIO n

    #Drivers -> Real Time Clock
    kernel_conf_variable RTC_DRV_DS1307 n

    #Drivers -> Staging drivers
    kernel_conf_variable STAGING n
    kernel_conf_variable ION n

    #Drivers -> Rpmsg drivers
    kernel_conf_variable IMX_RPMSG_PINGPONG n
    kernel_conf_variable IMX_RPMSG_TTY n

    #Drivers -> External connector class (excon)
    kernel_conf_variable EXTCON_USB_GPIO n

    #Drivers -> Industrial I/O
    kernel_conf_variable MPL3115 n

    #File Systems - CDROM
    kernel_conf_variable ISO9660_FS n
    kernel_conf_variable UDF_FS n

    #File Systems - DOS/FAT/NTFS
    kernel_conf_variable MSDOS_FS n
    kernel_conf_variable NTFS_FS y
}

do_compile_prepend () {
    cp ${WORKDIR}/tester.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/labtester.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/labtesterq.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/tester-core.dtsi ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/adv.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/adv-prog.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/advq.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/advq-prog.dts ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/adv-display.dtsi ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/adv-camera.dtsi ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/adv-core.dtsi ${S}/arch/arm/boot/dts
    cp ${WORKDIR}/isl7998x_mipi.c ${S}/drivers/media/platform/mxc/capture
    cp ${WORKDIR}/ds90ub954_mipi.c ${S}/drivers/media/platform/mxc/capture
    cp ${WORKDIR}/ds90ub953_mipi.c ${S}/drivers/media/platform/mxc/capture
    cp ${WORKDIR}/adv_wdt.c ${S}/drivers/watchdog/
}

