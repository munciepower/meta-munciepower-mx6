FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://v4l2src.c file://v4l2src.h"

do_compile_prepend () {
    cp ${WORKDIR}/v4l2src.c ${S}/src/v4l2video/
    cp ${WORKDIR}/v4l2src.h ${S}/src/v4l2video/
}

