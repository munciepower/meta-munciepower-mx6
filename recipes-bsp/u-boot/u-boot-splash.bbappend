
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://omni_splash.bmp"

do_install () {
	install -d ${D}/boot
	install -m 644 ${WORKDIR}/omni_splash.bmp ${D}/boot/splash.bmp
}
