LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit useradd

RDEPENDS_${PN} += "bash"
DEPENDS += "update-rc.d-native"

SRC_URI += "file://testinit \
			file://register.sh \
            file://UniversalCore.cfg \
            file://ca-certificates.crt"

USERADD_PACKAGES = "${PN}"

# password "xyz"
# openssl passwd xyz

USERADD_PARAM_${PN} = "-u 1000 -d /home/mppdebug -r -s /bin/bash -p p9dauWO6/UVig -G sudo mppdebug"

do_install(){
    install -d ${D}/core/
    install -g lp -d 0771 ${D}/core/cups/
    install -d ${D}/core/tests/
    install -d ${D}/core/citests/
    install -d ${D}/etc/
    install -d ${D}/etc/init.d/
    install -d ${D}/home/mppdebug/
    install -d ${D}/etc/default/
    install -d ${D}/etc/ssl/
    install -d ${D}/etc/ssl/certs/

    install -m 0755 ${WORKDIR}/testinit ${D}/etc/init.d/testinit
    install -m 0755 ${WORKDIR}/register.sh ${D}/core/register.sh
    install -m 0644 ${WORKDIR}/UniversalCore.cfg ${D}/etc/default/UniversalCore.cfg
    install -m 0644 ${WORKDIR}/ca-certificates.crt ${D}/etc/ssl/certs/ca-certificates.crt

    update-rc.d -r ${D} testinit start 50 5 . stop 01 0 1 6 .
}

FILES_${PN} += "/etc/init.d/coreinit /core/* /home/mppdebug /etc/sudoers.d/01_wheel /etc/default/UniversalCore.cfg /etc/ssl/certs/ca-certificates.crt"

