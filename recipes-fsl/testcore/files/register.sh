#!/bin/bash

    #get serial
    serial=`cat /sys/class/net/eth0/address`
    serial="${serial//:}"

    #register device
    openssl genrsa -des3 -out /etc/ssl/private/auth.key -passout pass:$serial 2048
    openssl rsa -in /etc/ssl/private/auth.key -outform PEM -pubout -passin pass:$serial -out /etc/ssl/auth.crt

    pubkey=`cat /etc/ssl/auth.crt`
    pubkey="${pubkey/-----BEGIN PUBLIC KEY-----/}"
    pubkey="${pubkey/-----END PUBLIC KEY-----/}"
    pubkey="$(echo -e $pubkey | tr -d '[:space:]')"

    echo $serial
	echo $pubkey
