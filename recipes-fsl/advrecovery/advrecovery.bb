LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

RDEPENDS_${PN} += "bash"
DEPENDS += "update-rc.d-native"

SRC_URI += "file://advinstall file://cert"


do_install(){
    install -d ${D}/etc/
    install -d ${D}/etc/init.d/
    install -d ${D}/deploy/

    install -m 0755 ${WORKDIR}/advinstall ${D}/etc/init.d/advinstall
    install -m 0755 ${WORKDIR}/cert ${D}/etc/cert

    update-rc.d -r ${D} advinstall start 99 5 . stop 01 0 1 6 .
}

FILES_${PN} += "/etc/init.d/advinstall /deploy"

