LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit useradd

RDEPENDS_${PN} += "bash"
DEPENDS += "update-rc.d-native"

SRC_URI += "file://style.css \
            file://AdvMain.cfg \
            file://advinit \
            "

USERADD_PACKAGES = "${PN}"

# password "xyz"
# openssl passwd xyz

USERADD_PARAM_${PN} = "-u 1000 -d /home/mppdebug -r -s /bin/bash -p p9dauWO6/UVig -G sudo mppdebug"

do_install(){
    install -d ${D}/advantage/
    install -d ${D}/advantage/backup/
    install -d ${D}/advantage/config/
    install -d ${D}/advantage/tmp/
    install -d ${D}/advantage/f/
    install -d ${D}/advantage/f/core/
    install -d ${D}/advantage/f/logs/
    install -d ${D}/advantage/f/video/
    install -d ${D}/advantage/f/camrecord/
    install -d ${D}/etc/
    install -d ${D}/etc/init.d/
    install -d ${D}/home/mppdebug

    install -m 0644 ${WORKDIR}/style.css ${D}/advantage/config/style.css
    install -m 0644 ${WORKDIR}/AdvMain.cfg ${D}/advantage/config/AdvMain.cfg
    install -m 0755 ${WORKDIR}/advinit ${D}/etc/init.d/advinit

    ln -s -r ${D}/sys/class/i2c-dev/i2c-0/device/0-0044 ${D}/advantage/dev-csi
    ln -s -r ${D}/sys/class/i2c-dev/i2c-0/device/0-0030 ${D}/advantage/dev-fpd

    update-rc.d -r ${D} advinit start 50 5 . stop 01 0 1 6 .
}

FILES_${PN} += "/etc/init.d/advinit /advantage/* /home/mppdebug /etc/sudoers.d/01_wheel"

