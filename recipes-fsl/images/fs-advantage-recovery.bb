#
# Copyright (C) 2019 Muncie Power Products, Inc
#

DESCRIPTION = "Advantage+ Image"
LICENSE = "MIT"

LICENSE_FLAGS_WHITELIST = "commercial"

inherit core-image distro_features_check populate_sdk_qt5

IMAGE_FEATURES += " \
    splash \
    package-management \
    ssh-server-dropbear \
    hwcodecs \
    debug-tweaks \
    nfs-server \
    tools-debug \
    tools-testapps \
"

CORE_IMAGE_EXTRA_INSTALL += " \
    qtbase \
	packagegroup-core-full-cmdline \
	packagegroup-tools-bluetooth \
	packagegroup-imx-tools-audio \
	packagegroup-fsl-tools-gpu \
	packagegroup-fsl-tools-gpu-external \
	packagegroup-fsl-tools-testapps \
	packagegroup-fsl-tools-benchmark \
	packagegroup-fsl-gstreamer1.0 \
	packagegroup-fsl-gstreamer1.0-full \
    i2c-tools \
	iperf3 \
	strace \
	screen \
	minicom \
	openssl \
    devmem2 \
    cryptopp \
    advfonts \
    advrecovery \
    curl \
    fboverlaytool \
	dosfstools \ 
"

CONFLICT_DISTRO_FEATURES = "directfb"

PACKAGECONFIG_append_pn-gstreamer1.0-plugins-bad = " faad"

IMAGE_FSTYPES_remove = "multiubi"
