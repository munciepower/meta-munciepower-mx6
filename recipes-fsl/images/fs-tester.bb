#
# Copyright (C) 2019 Muncie Power Products, Inc
#

DESCRIPTION = "UTS Image"
LICENSE = "MIT"

inherit core-image distro_features_check populate_sdk_qt5

IMAGE_FEATURES += " \
    ssh-server-dropbear \
    hwcodecs \
    debug-tweaks \
    nfs-server \
"

EXTRA_IMAGE_FEATURES +="read-only-rootfs"

CORE_IMAGE_EXTRA_INSTALL += " \
    qtbase \
	packagegroup-core-full-cmdline \
	packagegroup-tools-bluetooth \
    i2c-tools \
    iproute2 \
    mtd-utils \
    mtd-utils-ubifs \
    fsl-rc-local \
    ethtool \
    connman \
    connman-client \
    imx-kobs \
    vlan \
    cryptodev-module \
    cryptodev-tests \
    procps \
    iw \
    can-utils \
    nano \
    ntpdate \
    coreutils \
    mmc-utils \
    udev-extraconf \
	openssl \
    cryptopp \
    testcore \
    cups \
	advfonts \
    cups-filters \
    ghostscript \
	liberation-fonts \
	ttf-bitstream-vera \
    libhackrf \
    valgrind \
	dosfstools \ 
    python3-cffi \
    python3-libipcqueue \
"

# read_only_rootfs hook adds DROPBEAR_RSAKEY_DIR=/var/lib/dropbear/ to the /etc/default/dropbear overriding our value in the dropbear recipe for this
# this is not a great solution...
# also move localtime if read only rootfs
read_only_rootfs_hook_append() {
    sed -i '$ d' ${IMAGE_ROOTFS}/etc/default/dropbear
    ln -sf /usr/share/zoneinfo/US/Eastern ${IMAGE_ROOTFS}/etc/localtime


	mv ${IMAGE_ROOTFS}/var/spool/cups ${IMAGE_ROOTFS}/core/cups/spool/
	ln -sf /core/cups/spool ${IMAGE_ROOTFS}/var/spool/cups
	mv ${IMAGE_ROOTFS}/var/cache/cups ${IMAGE_ROOTFS}/core/cups/cache/
	ln -sf /core/cups/cache ${IMAGE_ROOTFS}/var/cache/cups
}

CONFLICT_DISTRO_FEATURES = "directfb"

IMAGE_FSTYPES_remove = "multiubi"
