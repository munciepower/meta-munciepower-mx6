#
# Copyright (C) 2019 Muncie Power Products, Inc
#

DESCRIPTION = "Advantage+ Image"
LICENSE = "MIT"

LICENSE_FLAGS_WHITELIST = "commercial"

inherit core-image distro_features_check populate_sdk_qt5

IMAGE_FEATURES += " \
    splash \
    ssh-server-dropbear \
    hwcodecs \
    debug-tweaks \
    nfs-server \
"

EXTRA_IMAGE_FEATURES +="read-only-rootfs"

CORE_IMAGE_EXTRA_INSTALL += " \
    qtbase \
    packagegroup-core-full-cmdline \
    packagegroup-tools-bluetooth \
    packagegroup-imx-tools-audio \
    packagegroup-fsl-tools-gpu \
    packagegroup-fsl-tools-gpu-external \
    packagegroup-fsl-gstreamer1.0 \
    packagegroup-fsl-gstreamer1.0-full \
    i2c-tools \
    iproute2 \
    mtd-utils \
    mtd-utils-ubifs \
    fsl-rc-local \
    ethtool \
    connman \
    connman-client \
    imx-kobs \
    vlan \
    cryptodev-module \
    cryptodev-tests \
    procps \
    iw \
    can-utils \
    nano \
    ntpdate \
    coreutils \
    mmc-utils \
    udev-extraconf \
    openssl \
    fpdlink \
    cryptopp \
    advfonts \
    advcore \
    fboverlaytool \
    dosfstools \
    freetype \ 
"

# read_only_rootfs hook adds DROPBEAR_RSAKEY_DIR=/var/lib/dropbear/ to the /etc/default/dropbear overriding our value in the dropbear recipe for this
# this is not a great solution...
# also move localtime if read only rootfs
read_only_rootfs_hook_append() {
    sed -i '$ d' ${IMAGE_ROOTFS}/etc/default/dropbear
    ln -sf /usr/share/zoneinfo/Universal ${IMAGE_ROOTFS}/advantage/config/localtime
    rm ${IMAGE_ROOTFS}/etc/localtime
    ln -sf /advantage/config/localtime ${IMAGE_ROOTFS}/etc/localtime
}

CONFLICT_DISTRO_FEATURES = "directfb"

PACKAGECONFIG_append_pn-gstreamer1.0-plugins-bad = " faad"

IMAGE_FSTYPES_remove = "multiubi"
